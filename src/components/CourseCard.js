/*import { Card, Row, Col } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';

export default function CourseCard() {
  return (
    <Row className="mt-3 mb-3">
        <Col xs={12} md={12}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Full Stack Development</h2>
                    </Card.Title>
                    <Card.Text>
                    	<h4>Description:</h4>
                        <p>This is a sample course offering</p><br/>

                    	<h4>Price:</h4>
                        <p>PhP 40,000</p>
                        <Button variant="primary">Enroll</Button>    
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    </Row>
  );
}*/

import { Card, Button } from 'react-bootstrap';
// import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';


export default function CourseCard({course}) {

 /*   // Checks to see if the data was successfully passed
    console.log(props);
    // Every component receives information in a form of an object
    console.log(typeof props);*/

    // Destructure the "course" properties into their own variables "course" to make the code even shorter.
    const {_id, name, description, price} = course;

    
    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components

    // syntax
        // const [ getter, setter ] = useState(initialGetterValue)
   /* const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);
    const [isOpen, setIsOpen] = useState(true);*/

    // Using the state hook returns an array with the first element being a value and the second element as a funciton that's used to change the value of the first element.

    // console.log(useState(0));

/*    function enroll(){
        if (seats > 0) {
            setCount(count + 1);
            console.log('Enrollees ' + count);
            setSeats(seats - 1);
            console.log('Seats: ' + seats)
        }else{
            alert('No seats available.')
        }
    }*/

   /* function enroll(){
        setCount(count + 1);
        console.log('Enrollees ' + count);
        setSeats(seats - 1);
        console.log('Seats: ' + seats)
    }*/

    // useEffect - allows us to instruct the app that the components needs to do something after render.


   /* useEffect(() => {
        if (seats === 0) {
            setIsOpen(false);
            alert("No more seats available.");
            document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true)
        }

        // will run anytime one of the values in the array of dependencies changes.
    }, [seats])*/

        // Three types of dependencies
            // No dependency - effect function will run everytime the components renders.
            // With dependency (empty array) - effect function will only run (one time) when the components mounts and unmounts.
            // With dependency - effect function will run anytime one of the values in the array of dependencies changes.

    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
    )
}

    // Check if the CourseCard component is getting the correct prop types.
    // PropTypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next.

    CourseCard.propTypes = {
        course: PropTypes.shape({
            // Define the properties and their expected types
            name: PropTypes.string.isRequired,
            description: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired
        })
    }