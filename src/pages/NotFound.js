/*import { Link } from 'react-router-dom';

export default function NotFound(){
	return(
		<div>
		<h1>Ooops! Page does not exist!</h1>
		<h4>Error 404: Page Not Found!</h4>
		<p>Go back to <Link to="/">Home</Link>.</p>
		</div>
	)
}*/

import Banner from '../components/Banner';

export default function NotFound(){

	const data = {
		title: "Error 404 - Page Not Found!",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"
	}
	return (
		<Banner data={data}/>
		)
}