import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

import { useNavigate, Navigate } from 'react-router-dom'; 

import Swal from 'sweetalert2';

export default function Login(props) {

	// Allows us to consume the User Context object and its properties to use for user validation
	const { user, setUser } = useContext(UserContext);

	// hook return a function that lets you navigate to components.
	const navigate = useNavigate();
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);

	function loginUser(e){

		e.preventDefault();

		// "fetch" method is used to send request in the server and load the received response in the webpage
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			// Sets the headers data of the 'request' object to be sent to the backend
			headers: {
				'Content-Type' : 'application/json'
			},
			
			// JSON.stringify converts object data into stringified JSON
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		
		// The first ".then" method from the "response" object to convert the data retrieved into JSON format to be used in our application
		.then(res => res.json())
		.then(data => {
			// We will received either a token or an error response
			console.log(data)

			// If no user information is found, the access property will not be available and will return undefined
			// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
			if (typeof data.access !== "undefined") {
				
				// The JWT will be used to retrieved user inforamtion across the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
				
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
			} else{
				Swal.fire({
					title: 'Authentication Failed!',
					icon: 'error',
					text: 'Please, check your login details and try again!'
				})
			}
		});

		// Set the email of the loginUser in the local storage
			// Syntax
			// localStorage.setItem('propertyName', value)
		// localStorage.setItem("email", email);

		// In this case, the key name is 'email' and the value is the value of the email variable. This code sets the value of 'email' key in the local storage to the value of the email variable.

		// setUser( { email: localStorage.getItem('email') } )

		setEmail("");
		setPassword("");
		// navigate('/');

		// alert("You are now logged in!");
	};

	const retrieveUserDetails = (token) => {

		// The token will be sent as part of the request's header information
		// We put "Bearer" in front of the tokento follow implementation standards for JWTs
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Global user state for validation across the whole app
			// Changes the global user "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		});
	};

	useEffect(() => {

		if ((email !== "" && password !== "")) {
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [email, password]);

	return (
		(user.id !== null) ?
			<Navigate to="/courses" />
		:
			<Form onSubmit={(e)=>loginUser(e)}>
				<h1>Login</h1>
				<Form.Group controlId="email">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email here"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type = "password"
						placeholder = "Password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>


				{ isActive ?	

					<Button variant="success my-3" type="submit" id="submitBtn">Login</Button>
					:

					<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Login</Button>

				}		
			</Form>

	);
}
