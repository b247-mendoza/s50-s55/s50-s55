import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react'; 
import UserContext from '../UserContext';

import { Navigate, useNavigate } from 'react-router-dom'; 

import Swal from 'sweetalert2';

export default function Register() {

	const { user, setUser } = useContext(UserContext);

	// State hooks to store values of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [password2, setPassword2] = useState("");

	// State to determine wheter submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	function registerUser(e) {
		fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				Swal.fire({
					title: 'Registration Successful!',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
				setFirstName("");
				setLastName("");
				setEmail("");
				setMobileNo("");
				setPassword("");
				setPassword2("");
				setIsActive(false);
				navigate("/login")
			} else {
				Swal.fire({
					title: 'Something wen wrong',
					icon: 'error',
					text: 'Please try again!'
				})
			}
		});
	};

	const emailExist = (e) => {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			
			body: JSON.stringify({email})
		})

		.then(res => res.json())
		.then(data => {
			
			// console.log(data)
			
			if ( data === true) {

				Swal.fire({
					title: 'Duplicate Email Found!',
					icon: 'error',
					text: 'Please provide another email or try to Log in!'
				})


			} else{
				registerUser()
			}
		});

	};


	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match.

		if ((firstName !== "" && lastName !== "" && email !== "" && mobileNo.length >= 11 && password !== "" && password2 !== "" && password.length >= 11 && password === password2)) {
			setIsActive(true);
		} 
	}, [firstName, lastName, email, mobileNo, password, password2]);

	return (

		(user.id !== null) ?
			<Navigate to="/" />
		:
			<Form onSubmit={(e)=>emailExist(e)}>
				<h1>Register</h1>
				<Form.Group controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter First Name"
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter Last Name"
						value={lastName}
						onChange={e => setLastName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="email">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email here"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="mobileNo">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control
						type="mobileNo"
						placeholder="Enter Mobile Number"
						value={mobileNo}
						onChange={e => setMobileNo(e.target.value)}
						required
					/>

				</Form.Group>


				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type = "password"
						placeholder = "Password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						Must be 11 characters or more.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control
						type = "password"
						placeholder = "Verify Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{ isActive ?	

					<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>

					:
					<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>
				}		
			</Form>

	);
};